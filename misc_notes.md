Parallel tracks:

*Not implemented yet!*

One fun effect is having two tracks play at the same time, that
parallel somehow (e.g. the same music, but one is played with lo-fi
electronic timbres, while the other has classical instruments) and
crossfade between them.

1. Have a "play all simultaneously" option

2. Use `crossfade_to_child` to swap between them

3. Maybe have a manual mixer export also
