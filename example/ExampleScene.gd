func _init():
    randomize() # remember to randomize if you want shuffle to work!

func _on_AmbientButton_pressed():
    get_node('MusicPlaylist').play_child('AmbientMusicPlaylist')

func _on_ExcitingButton_pressed():
    get_node('MusicPlaylist').play_child('ExcitingMusicPlaylist')

func _on_ExcitingButtonCross_pressed():
    get_node('MusicPlaylist').crossfade_to_child('ExcitingMusicPlaylist')

func _on_AmbientButtonCross_pressed():
    get_node('MusicPlaylist').crossfade_to_child('AmbientMusicPlaylist')
