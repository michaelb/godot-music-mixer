Godot Music Mixer
-----------------

Godot Music Mixer is a helper scene (MusicPlaylist.tscn) for creating
and managing music playlists, and also adaptive music for video games.

It supports shuffle, looping, crossfading, and nesting.

Originally made for the [puzzle game Sawdust](http://michaelb.org/sawdust/).

Usage
-----

1. Include MusicPlaylist in your project

2. Add as many songs (as StreamPlayers) as you want in the order you
want as children to the playlist

3. Turn on Autoplay, or manually call the `play` method to get going!

Advanced usage
--------------

The playlist themselves can be stacked in a tree fashion (see example),
so you can multiple different moods or tags of music, and then
crossfade between them. This is useful for adaptive music, and to avoid
jarring music changes.

It can be done to any level of nesting, so you could, for example, have
a single master MusicPlaylist, that contains playlists for every game
level (so that when changing levels you can crossfade), and each game
level could have both "action" and "passive" playlists that
individually are crossfaded between.

Exported options
----------------

* **Autoplay** - turn on to play as soon as it enters the scene
* **Prefer pause** - good for long running ambient tracks, this means
that if the playlist stops, it actually will just pause and next time
it plays it will resume from this location
* **Repeat one** - repeats a single track forever
* **Crossfade** - crossfade between tracks when switching
* **Shuffle** - turn on to shuffle tracks. *NOTE: Remember to
`randomize()` before using this feature!*.
* **Crossfade Style** - whether to crossfade both tracks, or just one
(generally, "Fade out only" and "Crossfade" are the most useful)


Example
-------

The "example" directory contains an example project with two buttons,
letting you switch between an "exciting" playlist and a "ambient"
playlist. The ambient playlist has "prefer pause" enabled

Both the code and example music clips are licensed under MIT.


TODO
----

There are a lot of features I want to add:

* Fade in for playing (so that when resuming without crossfade it can
  fade in)
* Easing function for crossfade
* Manual mixing: One fun effect is having two tracks play at the same
  time, that parallel somehow (e.g. the same music, but one is played
  with lo-fi electronic timbres, while the other has classical
  instruments) and crossfade between them. (see "misc notes")
* Rudimentary beat matching (you'd need to specify BPM, it'd just try
  to sync them up if an even multiple)
* More looping features

Pull requests are welcome!

